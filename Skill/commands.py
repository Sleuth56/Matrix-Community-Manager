# Opsdroid
from opsdroid.connector.matrix.events import MatrixStateEvent
from opsdroid.events import Message, UserInvite, JoinRoom
from opsdroid.matchers import match_event, match_regex, match_crontab, match_catchall
from opsdroid.skill import Skill

import re
import markdown
import argparse
import shlex

from datetime import datetime
import pytz

from . import resources

import logging
_LOGGER = logging.getLogger(__name__)

# Help message information.
helpMessage = """
Commands control parts of the bot. You use <code>--add</code> and <code>--delete</code> to make changes. <br>
Use <code>--list</code> to list the current configuration of that item. <br>
Commands are listed below with all parameters. A general rule, when deleting an item you only need the name. <br>
If you want to change a command just edit! <br>
The bot uses 24-hour time <code>Mon, 01 Jan 2021 24:01</code> and respects time zones. If you want to know what time zone the bot is using <code>!timezone</code> <br>
<br>
"""

# Command help messages.
#  'DMCommands': '!delete, !preview and !send are controls for direct messages with this bot.',
commandHelp = {
  '!help': 'Shows this help message<pre><code>!help</pre></code>',
  '!admin': 'Who can modify your bot.<pre><code>!admin -u [USERID]</pre></code>',
  '!autoreply': 'Regex based replies.<pre><code>!autoreply -n [NAME] -x [REGEX] -m [MESSAGE] -r [ROOMID optional]</pre></code>',
  '!announcement': 'Announce something to the world!<pre><code>!announcement -n [NAME] -m [MESSAGE] -t [Mon, 01 Jan 2000 01:01] -r [ROOMID1, ROOMID2]</pre></code>',
  '!filter': 'All the better to sort things with my dear.<pre><code>!filter -f [FILTER] -r [ROOMID optional]</pre></code>',
  '!pipe': 'Move messages from one room to an other.<pre><code>!pipe -n [NAME] -source [ROOMID] -dest [ROOMID]</pre></code>',
  '!roomid': 'Obtain the internal ID for the matrix room. Fulfills <code>--room</code><pre><code>!roomid</pre></code>',
  '!apply': "Apply changes to the bot's <code>configuration.toml</code> file with out restarting.<pre><code>!apply</pre></code>",
  '!ban': "Ban a user from all rooms MCM is in.<pre><code>!ban -u [USERID]</pre></code>",
  '!unban': "Unban a user from all rooms MCM is in.<pre><code>!unban -u [USERID]</pre></code>",
  }

# Regexes for seperating input strings
UserIDRegex = "@([^\s]+):([^\s]+)"
splitRegex = '( -{1,2}[^\s]+|=)'

# Init Argparse
parser = argparse.ArgumentParser(exit_on_error=False, add_help=False)

# Action Operators
parser.add_argument('-a', '--add', dest='add', action='append', type=str, required=False)
parser.add_argument('-d', '--delete', dest='delete', action='append', type=str, required=False)
parser.add_argument('-l', '--list', dest='list', action='append', type=str, required=False)

# Data flags
parser.add_argument('-n', '--name', dest='name', action='append', type=str, required=False)
parser.add_argument('-x', '--regex', dest='regex', action='append', type=str, required=False)
parser.add_argument('-f', '--filter', dest='filter', action='append', type=str, required=False)
parser.add_argument('-t', '--time', dest='time', action='append', type=str, required=False)
parser.add_argument('-m', '--message', dest='message', action='append', type=str, required=False)
parser.add_argument('-u', '--user', dest='UserID', action='append', type=str, required=False)
parser.add_argument('-r', '--room', dest='RoomID', action='append', type=str, required=False)

# Pipes
parser.add_argument('-source', '--source', dest='source', action='append', type=str, required=False)
parser.add_argument('-dest', '--dest', dest='destination', action='append', type=str, required=False)

# Finds all useful data like Times RoomIDs and so on. Returns a dictionary object with the found data.
def prepairData(skillConfig, message):
  # This is supposed to be shlex.quote but it doesn't work properly
  # string.text = string.text.replace("'", "\\'").replace('\\"', '\\"')

  # result = shlex.split(string.text) # THIS DOESN'T WORK!!!!! IT DOESN'T WORK WITH MULTIPULE WORDS FOR AN INPUT!
  # It gives this: ['-n', 'new', '-x', 'new|now', '-m', 'Its', 'a', 'new', 'year!']
  # Where as I need: ['-n', 'new', '-x', 'new|now', '-m', 'Its a new year!']
  # It needs to keep the data for an argument in the same str object.

  # Tokenizer
  result = re.split(splitRegex, message.text)
  for i in range(len(result)):
    result[i] = result[i].rstrip().lstrip()

  # Parse out the command.
  _LOGGER.info(result)
  command = result.pop(0).lower()
  if '!' not in command:
    _LOGGER.info("Command parsing error")
    return None
  else:
    _LOGGER.info(command)

  # Parse flags.
  try:
    args = parser.parse_args(result)
    _LOGGER.info(args)
  except:
    _LOGGER.info(f"parsing error \n{command}")
    return None

  # Check operator flag.
  if (args.delete):
    operator = 'Delete'
  elif (args.add):
    operator = 'Add'
  else:
    operator = 'List'

  # UserID
  if ('formatted_body' in message.raw_event['content'].keys() and args.UserID):
    for i in range(len(args.UserID)):
      matrixTo = re.search('(<a href="https://matrix.to/#/@)(.*)(">)(.*)(</a>)', message.raw_event['content']['formatted_body'])
      if (matrixTo.group(4) == args.UserID[i]):
        args.UserID[i] = f"@{matrixTo.group(2)}"

  # Return parsed values.
  return {
    "Operator": operator,
    "Sender": message.raw_event["sender"],
    "Admin" : False if not resources.isAdmin(message.raw_event["sender"], skillConfig) == True else True,
    "Raw": message.text,
    "Command": command,
    "Name": args.name,
    "Regex": args.regex,
    "Message": args.message,
    "UserID": args.UserID,
    "RoomID": args.RoomID,
    "Time": args.time,
    "Filter": args.filter,
    "Source": args.source,
    "Destination": args.destination,
  }

async def help(skillConfig, messageEvent, messageData, command = None):
  if (command != None):
    await messageEvent.respond(markdown.markdown(commandHelp[command]))
    return
  elif (messageData['Admin'] == True):
    string = f"{resources.helpBanner} {helpMessage} "
    for message in commandHelp.keys():
      string += f"{commandHelp[message]}"
    await messageEvent.respond(string)
    return

async def roomID(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  await messageEvent.respond(messageEvent.target)

async def listAdmins(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  output = '<pre><code>'
  for i in skillConfig['Admins']:
    output += f"{skillConfig['Admins'][i]}\n"
  output += '</code></pre>'

  await messageEvent.respond(markdown.markdown(output))

async def addAdmin(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['UserID'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  for user in range(len(messageData['UserID'])):
    skillConfig['Admins'][re.findall(UserIDRegex, str(messageData['UserID']))[user][0]] = messageData['UserID'][user]
    resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

    await listAdmins(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def delAdmin(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['UserID'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  for user in range(len(messageData['UserID'])):
    try:
      skillConfig['Admins'].pop(re.findall(UserIDRegex, str(messageData['UserID']))[user][0])
    except KeyError:
      await messageEvent.respond(f"{messageData['UserID'][0]} was not an admin! 😰")
      return

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listAdmins(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def listAutoReplies(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  output = '<pre><code>'
  keys = list(skillConfig['AutoReply'].keys())
  for i in range(len(skillConfig['AutoReply'])):
    output += f"{keys[i]} | {skillConfig['AutoReply'][keys[i]]['Regex']} | {skillConfig['AutoReply'][keys[i]]['Message']} | "
    if ('RoomID' in skillConfig['AutoReply'][keys[i]]):
      output += f"{skillConfig['AutoReply'][keys[i]]['RoomID']}\n"
    else:
      output += 'default\n'
  output += '</code></pre>'

  await messageEvent.respond(output)

async def addAutoReply(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Name', 'Regex', 'Message'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  NameRegMes = len(messageData['Name'])+len(messageData['Regex'])+len(messageData['Message'])
  if (not NameRegMes % 3 == 0):
    _LOGGER.info(f"{NameRegMes}")
    await messageEvent.respond("You need the same number of Names, messages, and regexs")
    return

  for i in range(len(messageData['Name'])):
    skillConfig['AutoReply'][messageData['Name'][i]] = {}
    skillConfig['AutoReply'][messageData['Name'][i]]["Regex"] = messageData['Regex'][i]
    skillConfig['AutoReply'][messageData['Name'][i]]["Message"] = messageData['Message'][i]

    try:
      skillConfig['AutoReply'][messageData['Name'][i]]["RoomID"] = messageData['RoomID'][i]
    except TypeError:
      _LOGGER.info("No RoomID")

    resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listAutoReplies(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def delAutoReply(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Name'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  for i in range(len(messageData['Name'])):
    try:
      skillConfig['AutoReply'].pop(messageData['Name'][i])
    except KeyError:
      await messageEvent.respond(f"{messageData['Name'][i]} was not an auto reply! 😰")

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listAutoReplies(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def listAnnouncements(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  output = '<pre><code>'
  keys = list(skillConfig['Announcement'].keys())
  for i in range(len(skillConfig['Announcement'])):
    output += f"{keys[i]} | {skillConfig['Announcement'][keys[i]]['Time']} | {skillConfig['Announcement'][keys[i]]['Message']} | "
    if ('RoomID' in skillConfig['Announcement'][keys[i]]):
      output += f"{skillConfig['Announcement'][keys[i]]['RoomID']}\n"
    else:
      output += 'default\n'
  output += '</code></pre>'

  await messageEvent.respond(output)

async def addAnnouncement(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Name', 'Message', 'Time', 'RoomID'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  NameTimeMessageRoomID = len(messageData['Name'])+len(messageData['Message'])+len(messageData['Time'])
  if (not NameTimeMessageRoomID % 3 == 0):
    _LOGGER.info(f"{NameTimeMessageRoomID}")
    await messageEvent.respond("You need the same number of Names, Times, regexs and RoomIDs")
    return

  for i in range(len(messageData['Name'])):
    skillConfig['Announcement'][messageData['Name'][i]] = {}
    skillConfig['Announcement'][messageData['Name'][i]]["Message"] = messageData['Message'][i]
    skillConfig['Announcement'][messageData['Name'][i]]["Time"] = messageData['Time'][i]
    skillConfig['Announcement'][messageData['Name'][i]]["RoomID"] = messageData['RoomID'][i].replace(", ", ",").split(",")

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)
  await listAnnouncements(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def delAnnouncement(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Name'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  for i in range(len(messageData['Name'])):
    try:
      skillConfig['Announcement'].pop(messageData['Name'][i])
    except KeyError:
      await messageEvent.respond(f"{messageData['Name'][i]} was not an announcement! 😰")

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listAnnouncements(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def listFilters(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  output = '<pre><code>'
  keys = list(skillConfig['Filter'].keys())
  for i in range(len(skillConfig['Filter'])):
    output += f"{skillConfig['Filter'][keys[i]]['Filter']} | {skillConfig['Filter'][keys[i]]['RoomID']}\n"
  output += '</code></pre>'

  await messageEvent.respond(output)

async def addFilter(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Filter'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  NameTimeMessageRoomID = len(messageData['Filter'])
  if (not NameTimeMessageRoomID % 1 == 0):
    _LOGGER.info(f"{NameTimeMessageRoomID}")
    await messageEvent.respond("You need the same number of Filters and RoomIDs")
    return

  for i in range(len(messageData['Filter'])):
    skillConfig['Filter'][messageData['Filter'][i]] = {}
    skillConfig['Filter'][messageData['Filter'][i]]["Filter"] = messageData['Filter'][i]

    if ("RoomID" in skillConfig['Filter'][messageData['Filter'][i]]):
      skillConfig['Filter'][messageData['Filter'][i]]["RoomID"] = messageData['RoomID'][i]
    else:
      skillConfig['Filter'][messageData['Filter'][i]]["RoomID"] = messageEvent.target

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listFilters(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def delFilter(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Filter'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  for i in range(len(messageData['Filter'])):
    try:
      skillConfig['Filter'].pop(messageData['Filter'][i])
    except KeyError:
      await messageEvent.respond(f"{messageData['Filter'][i]} was not a filter! 😰")

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listFilters(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)


async def listPipes(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  output = '<pre><code>'
  keys = list(skillConfig['Pipe'].keys())
  for i in range(len(skillConfig['Pipe'])):
    output += f"{keys[i]} | {skillConfig['Pipe'][keys[i]]['Source']} | {skillConfig['Pipe'][keys[i]]['Destination']}\n"
  output += '</code></pre>'

  await messageEvent.respond(output)

async def addPipe(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Name', 'Source', "Destination"])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  NameTimeMessageRoomID = len(messageData['Name']) + len(messageData['Source']) + len(messageData['Destination'])
  if (not NameTimeMessageRoomID % 3 == 0):
    _LOGGER.info(f"{NameTimeMessageRoomID}")
    await messageEvent.respond("You need the same number of Name, Sources and Destinations")
    return

  for i in range(len(messageData['Name'])):
    skillConfig['Pipe'][messageData['Name'][i]] = {}
    skillConfig['Pipe'][messageData['Name'][i]]["Source"] = messageData['Source'][i]
    skillConfig['Pipe'][messageData['Name'][i]]["Destination"] = messageData['Destination'][i]

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listPipes(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

async def delPipe(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  if (not resources.requirementsCheck(messageData, ['Name'])):
    await help(skillConfig, messageEvent, messageData, messageData['Command'])
    return

  for i in range(len(messageData['Name'])):
    try:
      skillConfig['Pipe'].pop(messageData['Name'][i])
    except KeyError:
      await messageEvent.respond(f"{messageData['Name'][i]} was not a filter! 😰")

  resources.writeToConfig(resources.skillRootConfigFile, skillConfig)

  await listPipes(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)


async def timezone(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  await messageEvent.respond(f'The current time zone is: <font color="red"><code>{skillConfig["General"]["TimeZone"]}</code>')

async def inviteAdmins(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  _LOGGER.info("!inviteadmins initiated.")
  keys = list(skillConfig['Admins'].keys())
  for i in range(len(skillConfig['Admins'])):
    try:
      await opsdroid.send(UserInvite(user_id=skillConfig['Admins'][keys[i]], target=messageEvent.target))
      _LOGGER.info(f"    Invited: {skillConfig['Admins'][keys[i]]}")
    except:
      _LOGGER.info(f"    {skillConfig['Admins'][keys[i]]} was already in the room")

async def fireAnnouncement(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  currentTime = datetime.now(pytz.timezone(resources.configFile['General']['TimeZone']))
  announcements = resources.configFile['Announcement']

  for count in range(len(announcements)):
    currentAnnouncement = announcements[list(announcements.keys())[count]]

    # Check if the current event should fired
    if (resources.isItTimeYet(currentAnnouncement['Time'])):
      _LOGGER.info(f"Timed Message: {currentAnnouncement['Message']} {currentAnnouncement['RoomID']} {currentTime.strftime(resources.timeCode)}")
      await opsdroid.send(Message(currentAnnouncement['Message'], target=currentAnnouncement['RoomID']))

async def ban(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  joined_rooms_data = await opsdroid.get_connector('matrix').connection.joined_rooms()
  _LOGGER.info(joined_rooms_data.rooms)

  if (isinstance(messageData['UserID'], list)):
    for room in joined_rooms_data.rooms:
      for userID in messageData['UserID']:
        if (userID == resources.mxid):
          await opsdroid.send(Message(f'Nice try but I won\'t ban myself. 😎', target=room))
          return
        try:
          await opsdroid.send(MatrixStateEvent(target=room, event_type='m.room.member', content={"membership": "ban"}, state_key=userID))
        except:
          await opsdroid.send(Message(f'I don\'t have permission to ban users. Please get the room owner to run this command in element.<br> <pre><code>/op {opsdroid.get_connector("matrix").mxid} 50</code></pre>', target=room))
  else:
    await help(skillConfig, messageEvent, messageData, messageData['Command'])

async def unban(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData):
  joined_rooms_data = await opsdroid.get_connector('matrix').connection.joined_rooms()
  _LOGGER.info(joined_rooms_data.rooms)

  if (isinstance(messageData['UserID'], list)):
    for room in joined_rooms_data.rooms:
      for userID in messageData['UserID']:
        try:
          await opsdroid.send(MatrixStateEvent(target=room, event_type='m.room.member', content={"membership": "leave"}, state_key=userID))
        except:
          await opsdroid.send(Message(f'I don\'t have permission to unban users. Please get the room owner to run this command in element.<br> <pre><code>/op {opsdroid.get_connector("matrix").mxid} 50</code></pre>', target=room))
  else:
    await help(skillConfig, messageEvent, messageData, messageData['Command'])

async def command_helper(opsdroid, opsdroidConfig, skillConfig, messageEvent):
  # Parse message text
  messageData = prepairData(skillConfig, messageEvent)
  _LOGGER.info(messageData)
  if (messageData == None):
    await help(skillConfig, messageEvent, messageData)
    return

  # Help message command
  if (re.search('(!help|!commands|help|hi|hello)$', messageData['Command'], re.IGNORECASE)):
    await help(skillConfig, messageEvent, messageData)
  elif (re.search('!roomID', messageData['Command'], re.IGNORECASE)):
    await roomID(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

  # Admin only commands
  elif(messageData['Admin'] == True):
    # Admin management commands
    if (re.search('!admin', messageData['Command'], re.IGNORECASE)):
      if (messageData['Operator'] == 'Add'):
        await addAdmin(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      elif (messageData['Operator'] == 'Delete'):
        await delAdmin(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      else:
        await listAdmins(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

    # Auto reply
    elif (re.search('!autoReply', messageData['Command'], re.IGNORECASE)):
      if (messageData['Operator'] == 'Add'):
        await addAutoReply(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      elif (messageData['Operator'] == 'Delete'):
        await delAutoReply(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      else:
        await listAutoReplies(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

    # Timed announcements 
    elif (re.search('!announcement', messageData['Command'], re.IGNORECASE)):
      if (messageData['Operator'] == 'Add'):
        await addAnnouncement(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      elif (messageData['Operator'] == 'Delete'):
        await delAnnouncement(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      else:
        await listAnnouncements(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

    # Filterged responces
    elif (re.search('!filter', messageData['Command'], re.IGNORECASE)):
      if (messageData['Operator'] == 'Add'):
        await addFilter(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      elif (messageData['Operator'] == 'Delete'):
        await delFilter(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      else:
        await listFilters(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)

    # Pipes
    elif (re.search('!pipe', messageData['Command'], re.IGNORECASE)):
      if (messageData['Operator'] == 'Add'):
        await addPipe(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      elif (messageData['Operator'] == 'Delete'):
        await delPipe(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
      else:
        await listPipes(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
    
    #Miscellaneous
    elif (re.search('!timezone', messageData['Command'], re.IGNORECASE)):
      await timezone(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
    elif (re.search('!inviteadmins', messageData['Command'], re.IGNORECASE)):
      await inviteAdmins(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
    elif (re.search("!fire", messageData['Command'], re.IGNORECASE)):
      await fireAnnouncement(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
    elif (re.search("!ban", messageData['Command'], re.IGNORECASE)):
      await ban(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)
    elif (re.search("!unban", messageData['Command'], re.IGNORECASE)):
      await unban(opsdroid, opsdroidConfig, skillConfig, messageEvent, messageData)