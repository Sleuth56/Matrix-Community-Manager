# Commands

## Flags, flags and more flags

## --name/-n

A single word that is used to reference an item.

### --time/-t

Time, isn't it wonderful.... and ANNOYING. This bot uses a custom time format to attempt to make time and time zones easier to express.
The time format is as follows. `Mon, 01 Jan 2021 01:01`
* The first part is the day of the week. **Mon, Tue, Wed, Thu, Fri, Sat, Sun** followed by a comma.
* The second part is the day of the month. 01-31. If the number is less than 10 put a zero in front.
* The third part is the month of the year. **Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec**
* The forth part is the year. I don't have much else to say about that.
* Last but not least is the hour and minute. Like the day of the month if either of them are less than 9 put a zero in front of it. Separate the hour and minute with a double colon (`:`).

### --message/-m

Text to be displayed by the bot.

### --regex/-x

A regex text string. https://regexr.com/

### --filter/-f

The symbol delimiter set in your configuration.toml file is used to denote a filter. It works very similar to hash tags on social media.

### --user/-u

Matrix ID. `@user:server.com`

### --room/-r

The unique identifier that we all call a room id. Isn't it great? I sure think so. Well it's also very confusing and there are two types of them.

This bot only supports the Actually unique and less confusing one. That also happens to be the one that people don't use because it's not easy to remember. I do have good news for you though. You can simply run `!roomid` in a room to get it's Unique ID. This is the ID that you will use in all commands that need it.

## !admin

### Parameters

* `-u` Full MXID.

### Example:

#### Add an admin
```bash
!admin -a -u @user:server.com
```

#### List admins
```bash
!admin -l
```

#### Remove an admin
```bash
!admin -d -u @user:server.com
```

## !announcement

### Parameters

* `-n` Sets the name of your announcement. This serves only as an internal name for you to reference in other commands or just at a glance.
* `-m` The message that the bot will send when the announcement goes live. The bot doesn't support markdown but it does support HTML. So if you want to give your message a bit of styling or color you can use HTML to do so.
* `-t` Time See **-t** under **Flags, flags and more flags**. It goes into great detail on how this flag works.
* `-r` RoomID See **-r** under **Flags, flags and more flags**. It goes into great detail on how this flag works.